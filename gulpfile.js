// Base
var gulp = require('gulp');
var watch = require('gulp-watch');
var browserSync = require('browser-sync');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var changed = require('gulp-changed');

// General
var pug = require('gulp-pug');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');

// Images
var tinypng = require('gulp-tinypng-compress');
var svgSprite = require('gulp-svg-sprites');
var replace = require('gulp-replace');
var cheerio = require('gulp-cheerio');
var base64 = require('gulp-base64');

// Styles
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');

var paths = {
	src: {
		self:			'./app',
		js:				'./app/js',
		sass:			'./app/sass',
        css: 			'./app/css',
        bootstrap:      './app/sass/bootstrap',
		libs:		    './app/sass/libs',
		images:			'./app/src',
		pug:			'./app/jade'
	},
	assets: {
		self:			'./dist/static',
		js:				'./dist/static/js',
		css:			'./dist/static/css',
		images:			'./dist/static/images'
	},
	node:				'node_modules/',
	html:				'./dist/'
}

gulp.task('browserSync', function() { 
	browserSync({ 
		server: { 
			port: 8000,
			baseDir: 'dist',
			directory: true
		}
	});
});

gulp.task('watch', function () {
    gulp.watch (paths.src.pug + '/*.pug', ['pug']);

    gulp.watch([paths.src.images + '/*.{png,jpg,jpeg}'], ['tinypng']);
    gulp.watch (paths.src.images + '/*.svg', ['svg']);
    gulp.watch (paths.src.images + '/static/*.svg', ['static-svg']);

    gulp.watch (paths.src.sass + '/*.scss', ['sass']);
    gulp.watch (paths.src.sass + '/components/*.scss', ['sass']);
    gulp.watch (paths.src.sass + '/containers/*.scss', ['sass']);
    gulp.watch (paths.src.css + '/libs/*.css', ['concat_css']);
});

gulp.task('pug', function () {
	gulp
		.src(paths.src.pug + '/*.pug')
		.pipe(plumber({ errorHandler: notify.onError("<%= error.message %>") }))
		.pipe(pug({pretty: '\t'}))
		.pipe(changed(paths.html, {extension: '.html', hasChanged: changed.compareContents}))
		.pipe(gulp.dest(paths.html))
})

gulp.task('tinypng', function () {
	gulp.src(paths.src.images + '/*.{png,jpg,jpeg}')
	.pipe(tinypng({
		key: 'RSo1T4ZVG0_f4fflJJ57krOzRTAEBW46',
		sigFile: paths.assets.images + '/.tinypng-sigs'
	}))
	.pipe(gulp.dest(paths.assets.images + ''))
});

gulp.task('svg', function () {
	gulp
		.src(paths.src.images + '/*.svg')
		.pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[style]').removeAttr('style');
            }
        }))
        .pipe(replace('&gt;', '>'))
        .pipe(
            svgSprite({
                mode: "symbols",
                preview: false,
                selector: "%f",
                svg: {
                symbols: 'sprite.svg' 
            },
            transformData: function (data, config) {
                for(var i in data.svg) {
                    var result = data.svg[i].data.match(/path id="([a-z]+)"/ig );
                    if (null !== result) {
                        for(var j in result) {
                            var regexp = /\"([a-z]+)\"/i;
                            var matches = regexp.exec(result[j]);
                            matches[0] = matches[0].replace(/\"/g, '');

                            var k = 0;

                            var regexp = new RegExp('(path id\=\"|xlink\:href\=\"#)('+matches[0]+'){1}', 'g');
                            data.svg[i].data = data.svg[i].data.replace(regexp, function(str, p1, p2, offset, s)
                                {
                                    return p1 + "" + i + "" + j + "" + p2;
                                });
                        }
                    }
                }
                return data;
            },
        }
        ))
        .pipe(replace('NaN ', '-'))
        .pipe(gulp.dest(paths.assets.images))
})

gulp.task('static-svg', function () {
	gulp
		.src(paths.src.images + '/static/*.svg')
        .pipe(replace('&gt;', '>'))
        .pipe(
            svgSprite({
                mode: "symbols",
                preview: false,
                selector: "%f",
                svg: {
                symbols: 'static-sprite.svg' 
            },
            transformData: function (data, config) {
                for(var i in data.svg) {
                    var result = data.svg[i].data.match(/path id="([a-z]+)"/ig );
                    if (null !== result) {
                        for(var j in result) {
                            var regexp = /\"([a-z]+)\"/i;
                            var matches = regexp.exec(result[j]);
                            matches[0] = matches[0].replace(/\"/g, '');

                            var k = 0;

                            var regexp = new RegExp('(path id\=\"|xlink\:href\=\"#)('+matches[0]+'){1}', 'g');
                            data.svg[i].data = data.svg[i].data.replace(regexp, function(str, p1, p2, offset, s)
                                {
                                    return p1 + "" + i + "" + j + "" + p2;
                                });
                        }
                    }
                }
                return data;
            },
        }
        ))
        .pipe(replace('NaN ', '-'))
        .pipe(gulp.dest(paths.assets.images))
})

gulp.task('sass', function () {
	gulp
        //.src([paths.src.sass + '/main.scss', paths.src.sass + '/containers/*.scss', paths.src.sass + '/components/*.scss'])
        .src([paths.src.sass + '/**/*.+(scss|sass)', '!' + paths.src.sass + '/libs/**/*.+(scss|sass)'])
		//.src(paths.src.sass + '/main.scss')
		.pipe(plumber({errorHandler: notify.onError("<%= error.message %>")}))
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'expanded'}))
		.pipe(base64({
			baseDir: 'dist/static/test',
			extensions: ['svg', 'png', /\.jpg#datauri$/i],
			exclude:    [/\.server\.(com|net)\/dynamic\//, '--live.jpg'],
			maxImageSize: 32*1024, // bytes 
			debug: false
		}))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(sourcemaps.write('./', {sourceRoot: '/source'}))
		.pipe(gulp.dest(paths.assets.css))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('concat_css', function() {
	gulp
		.src(paths.src.css + '/libs/*.css')
		.pipe(sourcemaps.init())
		.pipe(plumber({errorHandler: notify.onError("<%= error.message %>")})) 
		.pipe(csso())
		.pipe(concat('libs.css'))
		.pipe(sourcemaps.write('./', {sourceRoot: '/source'}))
		.pipe(gulp.dest(paths.assets.css))
});

gulp.task('run', ['browserSync', 'watch', 'pug', 'tinypng', 'svg', 'static-svg', 'sass', 'concat_css']);